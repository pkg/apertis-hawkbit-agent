/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include "hawkbit-deployment-base.h"
#include "hawkbit-session.h"

#include <json-glib/json-glib.h>

struct _AhaHawkbitDeploymentBase{
  GObject parent;
  AhaHawkbitSession *session;
  gchar *uri;

  gchar *update_uri;
  gchar *update_filename;
  gchar *deployment_id;

  JsonParser *parser;

};

enum
{
  PROP_HAWKBIT_SESSION = 1,
  PROP_URI,
  NUM_PROPERTIES
};
static GParamSpec *props[NUM_PROPERTIES] = { NULL, };


G_DEFINE_TYPE (AhaHawkbitDeploymentBase,
               aha_hawkbit_deployment_base,
               G_TYPE_OBJECT)

static void
aha_hawkbit_deployment_base_init (AhaHawkbitDeploymentBase * self)
{
}

/*
static void
aha_hawkbit_deployment_base_constructed (GObject *object)
{
  //AhaHawkbitDeploymentBase *self = AHA_HAWKBIT_DEPLOYMENT_BASE (object);
}
*/

static void
aha_hawkbit_deployment_base_dispose (GObject * object)
{
  AhaHawkbitDeploymentBase *self = AHA_HAWKBIT_DEPLOYMENT_BASE (object);

  g_clear_object (&self->session);
  g_clear_object (&self->parser);
  g_clear_pointer (&self->uri, g_free);
  g_clear_pointer (&self->update_uri, g_free);
  g_clear_pointer (&self->update_filename, g_free);
  g_clear_pointer (&self->deployment_id, g_free);

  if (G_OBJECT_CLASS (aha_hawkbit_deployment_base_parent_class)->dispose)
    G_OBJECT_CLASS (aha_hawkbit_deployment_base_parent_class)->dispose (object);
}


static void
aha_hawkbit_deployment_base_set_property (GObject *object,
                      guint prop_id,
                      const GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitDeploymentBase *self = AHA_HAWKBIT_DEPLOYMENT_BASE (object);

  switch (prop_id)
    {
      case PROP_HAWKBIT_SESSION:
        self->session = g_value_dup_object (value);
        break;
      case PROP_URI:
        self->uri = g_value_dup_string (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_deployment_base_get_property (GObject *object,
                      guint prop_id,
                      GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitDeploymentBase *self = AHA_HAWKBIT_DEPLOYMENT_BASE (object);

  switch (prop_id)
    {
      case PROP_HAWKBIT_SESSION:
        g_value_set_object (value, self->session);
        break;
      case PROP_URI:
        g_value_set_string (value, self->uri);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_deployment_base_class_init (AhaHawkbitDeploymentBaseClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  props[PROP_HAWKBIT_SESSION] =
    g_param_spec_object ("session",
                         "Hawkbit Session",
                         "Hawkbit Session Object",
                         AHA_TYPE_HAWKBIT_SESSION,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  props[PROP_URI] =
    g_param_spec_string ("uri",
                         "Hawkbit Deployment base uri",
                         "Hawkbit Deployment base uri",
                         "",
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  object_class->dispose = aha_hawkbit_deployment_base_dispose;
  object_class->get_property = aha_hawkbit_deployment_base_get_property;
  object_class->set_property = aha_hawkbit_deployment_base_set_property;
  //object_class->constructed = aha_hawkbit_deployment_base_constructed;

  g_object_class_install_properties (object_class,
                                     NUM_PROPERTIES,
                                     props);
}

gboolean
aha_hawkbit_deployment_base_update (AhaHawkbitDeploymentBase *self)
{
  g_autoptr  (SoupMessage) msg;
  guint status;
  JsonNode *root = NULL;
  JsonNode *node = NULL;
  JsonNode *anode = NULL;
  JsonObject *obj = NULL;
  JsonArray *array = NULL;

  g_clear_object (&self->parser);
  g_clear_pointer (&self->update_uri, g_free);
  g_clear_pointer (&self->update_filename, g_free);
  g_clear_pointer (&self->deployment_id, g_free);

  msg = aha_hawkbit_session_create_message_for_uri (self->session,
                                                    SOUP_METHOD_GET,
                                                    self->uri);
  status = soup_session_send_message (
    aha_hawkbit_session_get_soup_session (self->session),
    msg);
  if (status < 200 || status > 299)
    {
      g_debug ("%s: status=%d message='%s'", __func__, status,
                                             msg->response_body->data);
      return FALSE;
    }

  self->parser = json_parser_new();

  json_parser_load_from_data (self->parser,
                              msg->response_body->data,
                              msg->response_body->length,
                              NULL);

  root = json_parser_get_root (self->parser);
  /* retriee deployement id */
  obj = json_node_get_object (root);
  node = json_object_get_member (obj, "id");

  self->deployment_id = json_node_dup_string (node);

  /* retrieve update uri */
  obj = json_node_get_object (root);
  node = json_object_get_member (obj, "deployment");

  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "chunks");
  /* assume there is only one chunk with one artifact */
  array = json_node_get_array (node);
  node = json_array_get_element (array, 0);

  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "artifacts");

  array = json_node_get_array (node);
  node = json_array_get_element (array, 0);

  obj = json_node_get_object (node);

  anode = json_object_get_member (obj, "filename");
  self->update_filename = json_node_dup_string (anode);

  node = json_object_get_member (obj, "_links");

  /* Try to get HTTPS download URL in first intention, else try HTTP one */
  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "download");
  if (!node)
    node = json_object_get_member (obj, "download-http");
  if (!node)
    {
      g_debug("no download link");
      return FALSE;
    }

  obj = json_node_get_object (node);
  node = json_object_get_member (obj, "href");

  self->update_uri = json_node_dup_string (node);

  return TRUE;
}

gchar *
aha_hawkbit_deployment_base_get_update_uri (AhaHawkbitDeploymentBase *self)
{

  return g_strdup (self->update_uri);
}

gchar *
aha_hawkbit_deployment_base_get_update_filename (AhaHawkbitDeploymentBase *self)
{

  return g_strdup (self->update_filename);
}

void
aha_hawkbit_deployment_base_send_feedback (AhaHawkbitDeploymentBase *self,
                                                gchar *finished,
                                                gchar *execution,
                                                gchar *details)
{
  g_autoptr (SoupMessage) msg;
  guint status;
  g_autoptr (JsonBuilder) builder = json_builder_new ();

  json_builder_begin_object (builder);

  json_builder_set_member_name (builder, "id");
  json_builder_add_string_value (builder, self->deployment_id);

  json_builder_set_member_name (builder, "status");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "result");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "finished");
  json_builder_add_string_value (builder, finished);
  json_builder_end_object (builder);
  json_builder_set_member_name (builder, "execution");
  json_builder_add_string_value (builder, execution);
  if (details != NULL)
    {
      json_builder_set_member_name (builder, "details");
      json_builder_begin_array (builder);
      json_builder_add_string_value (builder, details);
      json_builder_end_array (builder);
    }
  json_builder_end_object (builder);

  json_builder_end_object (builder);

  g_autoptr (JsonGenerator) gen = json_generator_new ();
  g_autoptr (JsonNode) root = json_builder_get_root (builder);
  json_generator_set_root (gen, root);
  g_autofree gchar *str = json_generator_to_data (gen, NULL);

  g_autofree gchar *r = g_strdup_printf("/deploymentBase/%s/feedback",
                                        self->deployment_id);
  msg = aha_hawkbit_session_create_message(self->session, SOUP_METHOD_POST, r);
  soup_message_headers_append (msg->request_headers,
                               "Accept", "application/hal+json");
  soup_message_set_request (msg, "application/json;charset=UTF-8",
                            SOUP_MEMORY_COPY, str, strlen (str));
  status = soup_session_send_message (
                        aha_hawkbit_session_get_soup_session (self->session),
                        msg);
  if (status < 200 || status > 299)
    {
      g_debug ("%s: status=%d message='%s'", __func__, status,
                                             msg->response_body->data);
    }
}

/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include "hawkbit-session.h"

struct _AhaHawkbitSession{
  GObject parent;
  SoupSession *session;
  gchar *base_uri;
  gchar *tenant;
  gchar *controller_id;
  gchar *key_token;
};

enum
{
  PROP_BASE_URI = 1,
  PROP_TENANT,
  PROP_CONTROLLER_ID,
  PROP_KEY_TOKEN,
  NUM_PROPERTIES
};
static GParamSpec *props[NUM_PROPERTIES] = { NULL, };


G_DEFINE_TYPE (AhaHawkbitSession,
               aha_hawkbit_session,
               G_TYPE_OBJECT)

static void
aha_hawkbit_session_init (AhaHawkbitSession * self)
{
  self->session = soup_session_new_with_options (SOUP_SESSION_USER_AGENT,
                                                 "Apertis Hawkbit Agent",
                                                 NULL);

  if (g_getenv("SOUP_DEBUG"))
    {
      SoupLogger *logger;

      g_debug ("libsoup debug enabled\n");
      logger = soup_logger_new (SOUP_LOGGER_LOG_BODY, -1);
      soup_session_add_feature (self->session, SOUP_SESSION_FEATURE (logger));
      g_object_unref (logger);
    }
}

/*
static void
aha_hawkbit_session_constructed (GObject *object)
{
  //AhaHawkbitSession *self = AHA_HAWKBIT_SESSION (object);
}
*/

static void
aha_hawkbit_session_dispose (GObject * object)
{
  AhaHawkbitSession *self = AHA_HAWKBIT_SESSION (object);

  g_clear_object (&self->session);

  g_clear_pointer (&self->base_uri, g_free);
  g_clear_pointer (&self->tenant, g_free);
  g_clear_pointer (&self->controller_id, g_free);
  g_clear_pointer (&self->key_token, g_free);

  if (G_OBJECT_CLASS (aha_hawkbit_session_parent_class)->dispose)
    G_OBJECT_CLASS (aha_hawkbit_session_parent_class)->dispose (object);
}


static void
aha_hawkbit_session_set_property (GObject *object,
                      guint prop_id,
                      const GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitSession *self = AHA_HAWKBIT_SESSION (object);

  switch (prop_id)
    {
      case PROP_BASE_URI:
        self->base_uri = g_value_dup_string (value);
        break;
      case PROP_TENANT:
        self->tenant = g_value_dup_string (value);
        break;
      case PROP_CONTROLLER_ID:
        self->controller_id = g_value_dup_string (value);
        break;
      case PROP_KEY_TOKEN:
        self->key_token = g_value_dup_string (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_session_get_property (GObject *object,
                      guint prop_id,
                      GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitSession *self = AHA_HAWKBIT_SESSION (object);

  switch (prop_id)
    {
      case PROP_BASE_URI:
        g_value_set_string (value, self->base_uri);
        break;
      case PROP_TENANT:
        g_value_set_string (value, self->tenant);
        break;
      case PROP_CONTROLLER_ID:
        g_value_set_string (value, self->controller_id);
        break;
      case PROP_KEY_TOKEN:
        g_value_set_string (value, self->key_token);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_session_class_init (AhaHawkbitSessionClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  props[PROP_BASE_URI] =
    g_param_spec_string ("base-uri",
                         "Base uri",
                         "Base uri of the hawkbit server",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  props[PROP_TENANT] =
    g_param_spec_string ("tenant",
                         "Tenant",
                         "The tenant on the hawkbit server",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  props[PROP_CONTROLLER_ID] =
    g_param_spec_string ("controller-id",
                         "Controller Identifier",
                         "The controller on the hawkbit server",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  props[PROP_KEY_TOKEN] =
    g_param_spec_string ("key-token",
                         "Target Key Token",
                         "The key token of the target on the hawkbit server",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  object_class->dispose = aha_hawkbit_session_dispose;
  object_class->get_property = aha_hawkbit_session_get_property;
  object_class->set_property = aha_hawkbit_session_set_property;
  //object_class->constructed = aha_hawkbit_session_constructed;

  g_object_class_install_properties (object_class,
                                     NUM_PROPERTIES,
                                     props);
}

SoupMessage *
aha_hawkbit_session_create_message_for_uri (AhaHawkbitSession *self,
                                        const gchar *method,
                                        const gchar *uri)
{
  SoupMessage *msg = soup_message_new (method, uri);
  g_autofree gchar *t = g_strdup_printf("TargetToken %s", self->key_token);

  soup_message_headers_append (msg->request_headers, "Authorization", t);

  return msg;
}

SoupMessage *
aha_hawkbit_session_create_message (AhaHawkbitSession *self,
                                    const gchar *method,
                                    const gchar *resource)
{
  g_autofree gchar *u =
    g_strdup_printf ("%s/%s/controller/v1/%s%s",
                     self->base_uri,
                     self->tenant,
                     self->controller_id,
                     resource);
  return aha_hawkbit_session_create_message_for_uri (self, method, u);
}

SoupSession *
aha_hawkbit_session_get_soup_session (AhaHawkbitSession *self) {
        return self->session;
}

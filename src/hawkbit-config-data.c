/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include "hawkbit-config-data.h"
#include "hawkbit-session.h"

#include <json-glib/json-glib.h>

struct _AhaHawkbitConfigData{
  GObject parent;
  AhaHawkbitSession *session;
  gchar *uri;

  JsonParser *parser;
};

enum
{
  PROP_HAWKBIT_SESSION = 1,
  PROP_URI,
  NUM_PROPERTIES
};
static GParamSpec *props[NUM_PROPERTIES] = { NULL, };


G_DEFINE_TYPE (AhaHawkbitConfigData,
               aha_hawkbit_config_data,
               G_TYPE_OBJECT)

static void
aha_hawkbit_config_data_init (AhaHawkbitConfigData * self)
{
}

static void
aha_hawkbit_config_data_dispose (GObject * object)
{
  AhaHawkbitConfigData *self = AHA_HAWKBIT_CONFIG_DATA (object);

  g_clear_object (&self->session);
  g_clear_object (&self->parser);
  g_clear_pointer (&self->uri, g_free);

  if (G_OBJECT_CLASS (aha_hawkbit_config_data_parent_class)->dispose)
    G_OBJECT_CLASS (aha_hawkbit_config_data_parent_class)->dispose (object);
}


static void
aha_hawkbit_config_data_set_property (GObject *object,
                      guint prop_id,
                      const GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitConfigData *self = AHA_HAWKBIT_CONFIG_DATA (object);

  switch (prop_id)
    {
      case PROP_HAWKBIT_SESSION:
        self->session = g_value_dup_object (value);
        break;
      case PROP_URI:
        self->uri = g_value_dup_string (value);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_config_data_get_property (GObject *object,
                      guint prop_id,
                      GValue *value,
                      GParamSpec *pspec)
{
  AhaHawkbitConfigData *self = AHA_HAWKBIT_CONFIG_DATA (object);

  switch (prop_id)
    {
      case PROP_HAWKBIT_SESSION:
        g_value_set_object (value, self->session);
        break;
      case PROP_URI:
        g_value_set_string (value, self->uri);
        break;
      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
aha_hawkbit_config_data_class_init (AhaHawkbitConfigDataClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  props[PROP_HAWKBIT_SESSION] =
    g_param_spec_object ("session",
                         "Hawkbit Session",
                         "Hawkbit Session Object",
                         AHA_TYPE_HAWKBIT_SESSION,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  props[PROP_URI] =
    g_param_spec_string ("uri",
                         "Hawkbit Config data uri",
                         "Hawkbit Config data uri",
                         "",
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  object_class->dispose = aha_hawkbit_config_data_dispose;
  object_class->get_property = aha_hawkbit_config_data_get_property;
  object_class->set_property = aha_hawkbit_config_data_set_property;

  g_object_class_install_properties (object_class,
                                     NUM_PROPERTIES,
                                     props);
}

void
aha_hawkbit_config_data_send (AhaHawkbitConfigData *self)
{
  g_autoptr (SoupMessage) msg;
  guint status;
  g_autoptr (JsonBuilder) builder = json_builder_new ();

  g_message ("Send Config Data");

  json_builder_begin_object (builder);

  json_builder_set_member_name (builder, "data");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "HwRevision");
  json_builder_add_string_value (builder, "1.0");
  json_builder_end_object (builder);

  json_builder_set_member_name (builder, "status");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "result");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "finished");
  json_builder_add_string_value (builder, "none");
  json_builder_end_object (builder);
  json_builder_set_member_name (builder, "execution");
  json_builder_add_string_value (builder, "closed");
  json_builder_end_object (builder);

  json_builder_end_object (builder);

  g_autoptr (JsonGenerator) gen = json_generator_new ();
  g_autoptr (JsonNode) root = json_builder_get_root (builder);
  json_generator_set_root (gen, root);
  g_autofree gchar *str = json_generator_to_data (gen, NULL);

  msg = aha_hawkbit_session_create_message_for_uri(self->session,
                                                   SOUP_METHOD_PUT,
                                                   self->uri);
  soup_message_set_request (msg, "application/json;charset=UTF-8",
                            SOUP_MEMORY_COPY, str, strlen (str));
  status = soup_session_send_message (
                        aha_hawkbit_session_get_soup_session (self->session),
                        msg);
  if (status < 200 || status > 299)
    {
      g_debug ("%s: status=%d message='%s'", __func__, status,
                                            msg->response_body->data);
    }
}

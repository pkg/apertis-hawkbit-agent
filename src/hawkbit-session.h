/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AHA_HAWKBIT_SESSION_H__
#define __AHA_HAWKBIT_SESSION_H__

#include <glib-object.h>
#include <glib.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define AHA_TYPE_HAWKBIT_SESSION aha_hawkbit_session_get_type ()

G_DECLARE_FINAL_TYPE (AhaHawkbitSession,
                      aha_hawkbit_session,
                      AHA,
                      HAWKBIT_SESSION,
                      GObject)

SoupMessage *
aha_hawkbit_session_create_message_for_uri (AhaHawkbitSession *self,
                                            const gchar *method,
                                            const gchar *uri);

SoupMessage *
aha_hawkbit_session_create_message (AhaHawkbitSession *self,
                                    const gchar *method,
                                    const gchar *resource);

SoupSession *
aha_hawkbit_session_get_soup_session (AhaHawkbitSession *self);

G_END_DECLS

#endif /* __AHA_HAWKBIT_SESSION_H__ */


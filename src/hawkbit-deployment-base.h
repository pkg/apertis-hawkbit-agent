/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AHA_HAWKBIT_DEPLOYMENT_BASE_H__
#define __AHA_HAWKBIT_DEPLOYMENT_BASE_H__

#include <glib-object.h>
#include <glib.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define AHA_TYPE_HAWKBIT_DEPLOYMENT_BASE aha_hawkbit_deployment_base_get_type ()

G_DECLARE_FINAL_TYPE (AhaHawkbitDeploymentBase,
                      aha_hawkbit_deployment_base,
                      AHA,
                      HAWKBIT_DEPLOYMENT_BASE,
                      GObject)

#define AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_SUCCESS     "success"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_FAILURE     "failure"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_NONE        "none"

#define AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_CLOSED     "closed"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_PROCEEDING "proceeding"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_CANCELED   "canceled"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_SCHEDULED  "scheduled"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_REJECTED   "rejected"
#define AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_RESUMED    "resumed"

gboolean
aha_hawkbit_deployment_base_update (AhaHawkbitDeploymentBase *self);

gchar *
aha_hawkbit_deployment_base_get_update_uri (AhaHawkbitDeploymentBase *self);

gchar *
aha_hawkbit_deployment_base_get_update_filename (AhaHawkbitDeploymentBase *self);

void
aha_hawkbit_deployment_base_send_feedback (AhaHawkbitDeploymentBase *self,
                                                gchar *finished,
                                                gchar *execution,
                                                gchar *details);

G_END_DECLS

#endif /* __AHA_HAWKBIT_DEPLOYMENT_BASE_H__ */

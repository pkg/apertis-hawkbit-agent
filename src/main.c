/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

#include <stdio.h>
#include <gio/gio.h>

#include "hawkbit-session.h"
#include "hawkbit-base.h"
#include "hawkbit-deployment-base.h"
#include "hawkbit-config-data.h"

#define DEFAULT_POLL_TIMEOUT 10

typedef struct {
  GDBusProxy *proxy;

  AhaHawkbitSession *hb_session;
  AhaHawkbitBase *base;
  AhaHawkbitDeploymentBase *d_base;

  int poll_timeout;
  int poll_id;
} UpgradeAgent;

static void
download_got_chunk (SoupMessage *message,
                    SoupBuffer *buffer,
                    GOutputStream *out)
{
  g_autoptr (GBytes) bytes = soup_buffer_get_as_bytes (buffer);
  gsize size;
  gconstpointer d = g_bytes_get_data (bytes, &size);

  g_output_stream_write_all (out,
    d, size,
    NULL, NULL, NULL);
}

static void
download_uri (UpgradeAgent *agent, const char *uri, const gchar *filename)
{
  guint status;
  g_autoptr (SoupMessage) msg = aha_hawkbit_session_create_message_for_uri (
                                                        agent->hb_session,
                                                        SOUP_METHOD_GET,
                                                        uri);
  g_autoptr (GFile) f = g_file_new_for_path (filename);
  g_autoptr (GFileIOStream) io = g_file_replace_readwrite (f, NULL,
                                                        FALSE,
                                                        G_FILE_CREATE_NONE,
                                                        NULL, NULL);
  g_autoptr (GOutputStream) out = g_io_stream_get_output_stream (G_IO_STREAM (io));

  g_message ("Downloading %s\n", filename);
  soup_message_body_set_accumulate (msg->response_body, FALSE);
  g_signal_connect (msg, "got-chunk",
    G_CALLBACK (download_got_chunk),
    out);

  status = soup_session_send_message (
    aha_hawkbit_session_get_soup_session (agent->hb_session),
    msg);
  if (status < 200 || status > 299)
    {
      g_debug ("%s: status=%d message='%s'", __func__, status,
                                            msg->response_body->data);
    }
  g_output_stream_close (out, NULL, NULL);
}

static void aumCB(
    GDBusProxy * proxy,
    GVariant * changed_properties,
    GStrv invalidated_properties,
    gpointer user_data)
{
  UpgradeAgent *agent = (UpgradeAgent *)user_data;

  (void) invalidated_properties;

  if (agent->d_base == NULL)
    {
      // No update in progress.
      return;
    }

  if (g_variant_n_children(changed_properties) < 1)
    {
      // No changed properties, only invalidated ones which we don't care about.
      return;
    }

  /* Retrieve the org.apertis.ApertisUpdateManager property from the
     changed_properties dictionary parameter (index 1).
  */
  GVariant * const variant =
        g_variant_lookup_value(changed_properties, "SystemUpgradeState", NULL);
  if (variant != NULL)
    {
      int state;
      g_variant_get(variant, "u", &state);
      g_variant_unref(variant);
      g_debug ("SystemUpgradeState %d\n", state);

      if (state == 0)
        {
          aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_FAILURE,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_CLOSED,
                              NULL);
          g_object_unref(agent->d_base);
          agent->d_base = NULL;
        }
      else if (state == 100)
        {
          aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_NONE,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_PROCEEDING,
                              "Rebooting");
          aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_SUCCESS,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_CLOSED,
                              NULL);
          g_object_unref(agent->d_base);
          agent->d_base = NULL;
        }
      else if (state == 200)
        {
          aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_NONE,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_PROCEEDING,
                              "System already up to date");
          aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_SUCCESS,
                              AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_CLOSED,
                              NULL);
          g_object_unref(agent->d_base);
          agent->d_base = NULL;
        }
    }
}

static gboolean
static_delta_upgrade(UpgradeAgent *agent, const gchar *filename)
{
  g_autoptr(GError) error = NULL;

  g_dbus_proxy_call_sync (agent->proxy, "ApplyStaticDelta",
                          g_variant_new ("(s)", filename),
                          G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
  g_assert_no_error (error);

  return TRUE;
}

static void
deployment_base_mgmt(UpgradeAgent *agent, gchar *uri)
{
  g_autofree gchar *u_uri = NULL;
  g_autofree gchar *u_filename = NULL;

  if (agent->d_base != NULL)
    {
      g_message ("Update already in progress");
      return;
    }

  agent->d_base = g_object_new (AHA_TYPE_HAWKBIT_DEPLOYMENT_BASE,
                                "session", agent->hb_session,
                                "uri", uri, NULL);

  if (aha_hawkbit_deployment_base_update (agent->d_base) == FALSE)
    goto end;

  u_uri = aha_hawkbit_deployment_base_get_update_uri (agent->d_base);
  u_filename = g_strdup_printf("/tmp/%s",
                    aha_hawkbit_deployment_base_get_update_filename (agent->d_base));

  if (u_uri == NULL)
    goto end;

  aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                      AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_NONE,
                      AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_PROCEEDING,
                      "Downloading");

  download_uri (agent, u_uri, u_filename);

  // TODO: Check md5sum

  aha_hawkbit_deployment_base_send_feedback(agent->d_base,
                      AHA_DEPLOYMENT_BASE_FEEDBACK_FINISHED_NONE,
                      AHA_DEPLOYMENT_BASE_FEEDBACK_EXECUTION_PROCEEDING,
                      "Upgrading");

  static_delta_upgrade (agent, u_filename);

  return;

end:
  g_object_unref(agent->d_base);
  agent->d_base = NULL;
}

static void
config_data_mgmt(UpgradeAgent *agent, gchar *uri)
{
  AhaHawkbitConfigData *c_data = g_object_new (AHA_TYPE_HAWKBIT_CONFIG_DATA,
                                               "session", agent->hb_session,
                                               "uri", uri,
                                               NULL);

  aha_hawkbit_config_data_send(c_data);
  g_object_unref(c_data);
}

static gboolean
poll_cb (gpointer user_data)
{
  UpgradeAgent *agent = (UpgradeAgent *)user_data;
  g_autofree gchar *c_uri = NULL;
  g_autofree gchar *d_uri = NULL;
  int timeout = DEFAULT_POLL_TIMEOUT;

  if (aha_hawkbit_base_update (agent->base) == TRUE)
    {
      c_uri = aha_hawkbit_base_get_config_data (agent->base);
      if (c_uri != NULL)
        config_data_mgmt(agent, c_uri);

      d_uri = aha_hawkbit_base_get_deployment_base (agent->base);
      if (d_uri != NULL)
        deployment_base_mgmt(agent, d_uri);

      timeout = aha_hawkbit_base_get_polling_sleep(agent->base);
      if (timeout < 0)
        timeout = DEFAULT_POLL_TIMEOUT;
    }

  if (timeout != agent->poll_timeout)
    {
      agent->poll_timeout = timeout;
      g_message ("Changing poll timeout to %d seconds", agent->poll_timeout);
      agent->poll_id = g_timeout_add_seconds (agent->poll_timeout, poll_cb, agent);
      return G_SOURCE_REMOVE;
    }

  return G_SOURCE_CONTINUE;
}

int
main(int argc, char **argv) {
  UpgradeAgent *agent;
  g_autoptr(GError) error = NULL;
  g_autoptr(GKeyFile) key_file = g_key_file_new ();
  GMainLoop *loop;

  // Retrieve hawkBit update server configuration file for the target
  if (!g_key_file_load_from_file (key_file, "/etc/apertis-hawkbit-agent.ini", G_KEY_FILE_NONE, &error))
    {
      g_warning ("Error loading key file: %s", error->message);
      return 1;
    }

  g_autofree gchar *base_uri = g_key_file_get_string (key_file, "server",
                                                      "base-uri", &error);
  if (base_uri == NULL)
    {
      g_warning ("Error finding key in key file: %s", error->message);
      return 1;
    }

  g_autofree gchar *tenant = g_key_file_get_string (key_file, "server",
                                                    "tenant", &error);
  if (tenant == NULL)
    {
      if (!g_error_matches (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_KEY_NOT_FOUND))
        {
          g_warning ("Error finding key in key file: %s", error->message);
          return 1;
        }
      else
        {
          // Fall back to a default value.
          tenant = g_strdup ("DEFAULT");
          g_error_free(error);
          error = NULL;
        }
    }

  g_autofree gchar *controller_id = g_key_file_get_string (key_file, "server",
                                                           "controller-id", &error);
  if (controller_id == NULL)
    {
      g_warning ("Error finding key in key file: %s", error->message);
      return 1;
    }

  g_autofree gchar *key_token = g_key_file_get_string (key_file, "server",
                                                       "key-token", &error);
  if (key_token == NULL)
    {
      g_warning ("Error finding key in key file: %s", error->message);
      return 1;
    }

  agent = g_new0 (UpgradeAgent, 1);
  if (agent == NULL)
    {
      g_warning ("Failed to allocate Upgrade agent");
      return 1;
    }

  // Create AUM DBus proxy
  agent->proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SYSTEM,
                                 G_DBUS_PROXY_FLAGS_NONE, NULL,
                                 "org.apertis.ApertisUpdateManager",
                                 "/",
                                 "org.apertis.ApertisUpdateManager",
                                 NULL, &error);
  g_assert_no_error (error);

  g_signal_connect(agent->proxy, "g-properties-changed", G_CALLBACK(aumCB), agent);

  agent->hb_session = g_object_new (AHA_TYPE_HAWKBIT_SESSION,
                                    "base-uri", base_uri,
                                    "tenant", tenant,
                                    "controller-id", controller_id,
                                    "key-token", key_token,
                                    NULL);
  agent->base = g_object_new (AHA_TYPE_HAWKBIT_BASE,
                              "session", agent->hb_session,
                              NULL);

  agent->poll_timeout = DEFAULT_POLL_TIMEOUT;
  agent->poll_id = g_timeout_add_seconds (agent->poll_timeout, poll_cb, agent);

  loop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (loop);

  g_object_unref (agent->proxy);

  return 0;
}

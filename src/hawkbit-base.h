/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2019 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AHA_HAWKBIT_BASE_H__
#define __AHA_HAWKBIT_BASE_H__

#include <glib-object.h>
#include <glib.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define AHA_TYPE_HAWKBIT_BASE aha_hawkbit_base_get_type ()

G_DECLARE_FINAL_TYPE (AhaHawkbitBase,
                      aha_hawkbit_base,
                      AHA,
                      HAWKBIT_BASE,
                      GObject)

gboolean
aha_hawkbit_base_update (AhaHawkbitBase *self);

gchar *
aha_hawkbit_base_get_deployment_base (AhaHawkbitBase *self);

gchar *
aha_hawkbit_base_get_config_data (AhaHawkbitBase *self);

int
aha_hawkbit_base_get_polling_sleep (AhaHawkbitBase *self);

G_END_DECLS

#endif /* __AHA_HAWKBIT_BASE_H__ */
